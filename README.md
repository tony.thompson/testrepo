WMTP DHA SDK
======

## Build/Editing Process
1. Making changes to code
2. Run **Gulp**
    * Builds minified CSS and JS files for project
3. Git Push to repo branch
4. Deploy to Azure
    * Process done automatically when a push to repo is made
## Edit Styling
Styling is handled with **SASS** and is split among multiple files in the scss folder
## Sync Deploy to Azure
Through **webhooks** Azure is able to deploy the code from a Bitbucket Repository.
Manual Method: To sync changes to Azure hosting, on Azure navigate to App Services -> Deployment Slots -> Select service -> Deployment Center -> Sync

#### Automated Method: 
1. Azure
    1. Navigate to **App Service** and desired **Deployment Slot**
    2. Navigate to **Properties** and find **Deployment Trigger URL**
2. Bitbucket _(Must be an admin of repo to access Settings page)_
    1. Navigate to **repo -> Settings -> Webhooks**
    2. Add a Webhook by entering a Title and Deployment Trigger URL
    3. Can be configured for when to trigger the sync, by default it activates on a push
## Prerequisites/Troubleshooting
* Windows
    * Node-gyp
        * https://github.com/nodejs/node-gyp
    * Python
    * Visual Studio build tools

