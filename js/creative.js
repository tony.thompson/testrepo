/*
creative.js

This is where main logic for the creative bootstrap template is performed.

Modified by Tony Thompson on 1/10/20.

DHA SDK

Copyright © 2009-2019 United States Government as represented by
the Program Manager of the DHA: Web and Mobile Technology Program Management Office.
All Rights Reserved.

Copyright © 2009-2019 Contributors. All Rights Reserved.

THIS OPEN SOURCE AGREEMENT ("AGREEMENT") DEFINES THE RIGHTS OF USE,
REPRODUCTION, DISTRIBUTION, MODIFICATION AND REDISTRIBUTION OF CERTAIN
COMPUTER SOFTWARE ORIGINALLY RELEASED BY THE UNITED STATES GOVERNMENT
AS REPRESENTED BY THE GOVERNMENT AGENCY LISTED BELOW ("GOVERNMENT AGENCY").
THE UNITED STATES GOVERNMENT, AS REPRESENTED BY GOVERNMENT AGENCY, IS AN
INTENDED THIRD-PARTY BENEFICIARY OF ALL SUBSEQUENT DISTRIBUTIONS OR
REDISTRIBUTIONS OF THE SUBJECT SOFTWARE. ANYONE WHO USES, REPRODUCES,
DISTRIBUTES, MODIFIES OR REDISTRIBUTES THE SUBJECT SOFTWARE, AS DEFINED
HEREIN, OR ANY PART THEREOF, IS, BY THAT ACTION, ACCEPTING IN FULL THE
RESPONSIBILITIES AND OBLIGATIONS CONTAINED IN THIS AGREEMENT.

Government Agency: DHA: Web and Mobile Technology Program Management Office
Government Agency Original Software Designation:
Government Agency Original Software Title:
User Registration Requested. Please send email
with your contact information to: robert.a.kayl.civ@mail.mil
Government Agency Point of Contact for Original Software - Program Manager: robert.a.kayl.civ@mail.mil
 */

(function($) {
  "use strict"; // Start of use strict

  // Move the cards up to show a little dha red bros
  var moveUpCard = function () {
    $(".card").hover(function() {
      $(this).css("box-shadow", "2px 2px 8px #343a40");
    }, function(){
      $(this).css("box-shadow", "none");
    });
  };

  var getCurrentDate = function () {
    var month = new Array();
    month[0] = "January";
    month[1] = "February";
    month[2] = "March";
    month[3] = "April";
    month[4] = "May";
    month[5] = "June";
    month[6] = "July";
    month[7] = "August";
    month[8] = "September";
    month[9] = "October";
    month[10] = "November";
    month[11] = "December";
    var fullDate = new Date();
    var currentMonth =month[fullDate.getMonth()];
    var twoDigitDate = fullDate.getDate()+"";if(twoDigitDate.length==1)	twoDigitDate="0" +twoDigitDate;
    var currentDate = twoDigitDate + " " + currentMonth + ", " + fullDate.getFullYear();
    $("#updatedDate").html(currentDate);
  }


  // Perform tasks when document is ready
  var setUrls = function () {
    var locationOrigin = location.origin;
    var dhasdk = "https://dhasdk"
    var ifDev = "";
    var azure = ".azurewebsites.us";
    var bitbucket = "https://bitbucket.org/wmtp/";
    var readMe = "/src/master/README.md";
    var regEx = /-dev/;

    //Make changes for if in dev environment
    if (regEx.test(locationOrigin)) {
      readMe = "/src/dev/README.md";
      ifDev = "-dev";
      document.title = document.title + " (dev)";
    }

    // Nav
    $(".homeLink").prop("href", locationOrigin + "/#page-top");
    $("#aboutNavLink").prop("href", locationOrigin + "/#about");
    $("#packagesNavLink").prop("href", locationOrigin + "/#packages");
    $("#applicationsNavLink").prop("href", locationOrigin + "/#applications");
    $("#resourcesNavLink").prop("href", locationOrigin + "/#resources");
    $("#wmtApplicationsNavLink").prop("href", locationOrigin + "/wmt-applications.html");
    $("#tutorialNavLink").prop("href", dhasdk + "-tutorial" + ifDev + azure);

    //Packages
    // set Pouch Crypto url
    $("#pouchcrypto").prop("href", bitbucket + "dha-pouch-crypto" + readMe);
    // set oidc url
    $("#oidc-client").prop("href", bitbucket + "oidc-client-js" + "/src/master/README.md");
    // set security url
    $("#security").prop("href", bitbucket + "dha-security" + readMe);
    // set Database url
    $("#database").prop("href", bitbucket + "dha-db" + readMe);
    // set Encryption url
    $("#encryption").prop("href", bitbucket + "dha-encryption" + readMe);
    // set different themes url
    $("#themes").prop("href", bitbucket + "dha-theme" + readMe);
    // set the analytics nav link
    $("#analytics").prop("href", bitbucket + "dha-analytics" + readMe);
    // set Breathing Exercise url
    $("#breathingExercise").prop("href", bitbucket + "dha-breathing-exercise" + readMe);
    // set Graph url
    $("#graph").prop("href", bitbucket + "dha-graph" + readMe);
    // set Slider url
    $("#slider").prop("href", bitbucket + "dha-slider" + readMe);
    // set Assets React docs url
    $("#reactAssets").prop("href", bitbucket + "dha-assets" + readMe);
    // set Assets Typescript docs url
    $("#typescriptAssets").prop("href", bitbucket + "dha-assets" + readMe);
    // set login url
    $("#login").prop("href", bitbucket + "dha-login" + readMe);
    // dha-rating
    $("#dha-rating").prop("href", bitbucket + "dha-rating" + readMe);
    // dha-sms-notifications
    $("#dha-sms-notifications").prop("href", bitbucket + "dha-sms-notifications" + readMe);
    // dha-apple-auth
    $("#appleAuth").prop("href", bitbucket + "dha-apple-auth" + readMe);
    
    //Sample Applications
    // set the mood tracker app link
    $("#moodTrackerPWA").prop("href", dhasdk + "-moodtracker" + ifDev + azure);
    // set the mood tracker app link
    $("#breathToRelaxPWA").prop("href", dhasdk + "-b2r" + ifDev + azure);
    // set the PWA Starter doc url 
    $("#pwaStarter").prop("href", dhasdk + "-pwastarter" + ifDev + azure);
    // set the PWA Starter source code url
    $("#pwaStarterSourceCode").prop("href", bitbucket + "pwa-starter" + readMe);
    // set the MTF Template doc url
    $("#mtfTemplate").prop("href", dhasdk + "-mtftemplate" + ifDev + azure);

    // set the tutorial url
    $("#tutorialPage").prop("href", dhasdk + "-tutorial" + ifDev + azure);
  };

  setUrls();
  moveUpCard();
  getCurrentDate();

  // Animate Revision History
  $("#revHist").click(function() {
    $("#revHistToggle").toggle("slow", function() {
        $("html, body").animate({ scrollTop: $(document).height()-$(window).height() }, "slow");
    });
  })

  // Smooth scrolling using jQuery easing
  $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function () {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: (target.offset().top - 56)
        }, 1000, "easeInOutExpo");
        return false;
      }
    }
  });

  // Closes responsive menu when a scroll trigger link is clicked
  $('.js-scroll-trigger').click(function () {
    $('.navbar-collapse').collapse('hide');
  });

  // Activate scrollspy to add active class to navbar items on scroll
  $('body').scrollspy({
    target: '#mainNav',
    offset: 57
  });

  // Collapse Navbar
  var navbarCollapse = function () {
    if ($("#mainNav").offset().top > 100) {
      $("#mainNav").addClass("navbar-shrink");
    } else {
      $("#mainNav").removeClass("navbar-shrink");
    }
  };
  // Collapse now if page is not at top
  navbarCollapse();
  // Collapse the navbar when page is scrolled
  $(window).scroll(navbarCollapse);

  // Collapse React Modules Page Navbar
  var reactModulesNavbarCollapse = function () {
    if ($("#mainNav").offset().top > 100) {
      $("#mainNav").addClass("navbar-shrink");
    } else {
      $("#mainNav").removeClass("navbar-shrink");
    }
  };
  // Collapse now if page is not at top
  reactModulesNavbarCollapse();
  // Collapse the navbar when page is scrolled
  $(window).scroll(reactModulesNavbarCollapse);

  // Scroll reveal calls
  window.sr = ScrollReveal();

  sr.reveal('.sr-icon-1', {
    delay: 200,
    scale: 0
  });
  sr.reveal('.sr-icon-2', {
    delay: 400,
    scale: 0
  });
  sr.reveal('.sr-icon-3', {
    delay: 600,
    scale: 0
  });
  sr.reveal('.sr-icon-4', {
    delay: 800,
    scale: 0
  });
  sr.reveal('.sr-button', {
    delay: 200,
    distance: '15px',
    origin: 'bottom',
    scale: 0.8
  });
  sr.reveal('.sr-contact-1', {
    delay: 200,
    scale: 0
  });
  sr.reveal('.sr-contact-2', {
    delay: 400,
    scale: 0
  });

  // Magnific popup calls
  $('.popup-gallery').magnificPopup({
    delegate: 'a',
    type: 'image',
    tLoading: 'Loading image #%curr%...',
    mainClass: 'mfp-img-mobile',
    gallery: {
      enabled: false,
      navigateByImgClick: true,
      preload: [0, 1]
    },
    image: {
      tError: '<a href="%url%">The image</a> could not be loaded.'
    }
  });

})(jQuery); // End of use strict

function validateContactForm() {
  var errors = [{name: false}, {email: false}, {message: false}];

  if(!validateContactName()){
    // Name is not valid. Turn on error switch.
    errors[0].name = true;
  }
  if(!validateContactEmail()){
    // Email is not valid. Turn on error switch.
    errors[1].email = true;
  }
  if(!validateContactMessage()){
    // Message is not valid. Turn on error switch.
    errors[2].message = true;
  }
  if(checkForErrors(errors)){
    $('#contactSubmit').removeAttr("data-dismiss").removeAttr("data-toggle").removeAttr("data-target");
    return false;
  }
  else {
    $('#contactSubmit').attr("data-dismiss", "modal").attr("data-toggle", "modal").attr("data-target", "#contactConfirmationModal");
    return true;
  }
}

function checkForErrors (errors) {

  var hasErrors = false;

  for (var key in errors) {
    if((errors[key].name === true) || (errors[key].email === true) || (errors[key].message === true)){
      hasErrors = true;
    }
  }
  return hasErrors;
}

function validateContactName() {
  var contactName = $("#contactName")
  var contactNameValue = contactName.val();

  if(contactNameValue === ""){
    contactName.css('border-color', 'red');
    return false;
  }
  else {
    contactName.css('border-color', '');
    return contactNameValue;
  }
}

function validateContactEmail() {
  var contactEmail = $("#contactEmail");
  var contactEmailValue = contactEmail.val();

  if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(contactEmailValue))
  {
    contactEmail.css('border-color', '');
    return (true)
  }
  else {
    contactEmail.css('border-color', 'red');
    return (false)
  }
}

function validateContactMessage() {
  var contactMessage = $("#contactMessage");
  var contactMessageValue = contactMessage.val();

  if(contactMessageValue === ""){
    contactMessage.css('border-color', 'red');
    return false;
  }
  else {
    contactMessage.css('border-color', '');
    return contactMessageValue;
  }
}
