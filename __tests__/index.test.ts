const fs = require('fs');
const path = require('path');
const html = fs.readFileSync(path.resolve(__dirname, '../index.html'), 'utf8');

describe('title', function () {
  beforeEach(() => {
    document.documentElement.innerHTML = html;
  });

  afterEach(() => {
    // restore the original func after test
    jest.resetModules();
  });

  it('title exists', function () {
    expect(document.getElementById('title')).toBeTruthy();
  });

});

describe('navBar', function () {
  beforeEach(() => {
    document.documentElement.innerHTML = html;
  });

  afterEach(() => {
    // restore the original func after test
    jest.resetModules();
  });

  it('navbar exists', function () {
    expect(document.getElementById('navbarResponsive')).toBeTruthy();
  });

  it('contains all links', function () {
    let links = ["About","Packages","Sample Applications", "Resources", "WMT Applications", "Get Started: Tutorial",
    "Contact Us"];
    let ul = document.getElementById("navBar");
    let items = ul.getElementsByTagName("li");


    for (let i = 0; i < items.length; i++){
      let itemHTML = items[i];
      let itemElement = document.createElement("div");
      itemElement.innerHTML = itemHTML.innerHTML;
      let actualNavItem = itemElement.textContent.trim();
      let savedNavItem = links[i].trim();

      if(actualNavItem != savedNavItem) {
        console.log(actualNavItem);
        console.log(savedNavItem);
         throw new Error('The categories do not match!')
      }
    }

  });
});